﻿using System;

namespace Shared
{
    public class ResultadoRetorno<T>
    {
        public T Retorno { get; set; }

        public ResultadoEnum Resultado { get; set; }

        public string Message { get; set; }

        public string MessageTrace { get; set; }

        public string ErrorCode { get; set; }

        public decimal TotalElementos { get; set; }

        public bool IsOk
        {
            get
            {
                return Resultado == ResultadoEnum.Ok;
            }
        }

        public ResultadoRetorno()
        {
            Resultado = ResultadoEnum.Ok;
            Retorno = default(T);
        }

        public ResultadoRetorno(T instance)
        {
            Resultado = ResultadoEnum.Ok;
            Retorno = instance;
        }


        public ResultadoRetorno(string message, string messageTrace, string errorCode)
        {
            Resultado = ResultadoEnum.Error;
            Message = message;
            MessageTrace = messageTrace;
            ErrorCode = errorCode;
        }

        public void SetError(string message)
        {
            Resultado = ResultadoEnum.Error;
            Message = message;
        }

        public void SetError(string message, string messageTrace)
        {
            Resultado = ResultadoEnum.Error;
            Message = message;
            MessageTrace = messageTrace;
            ErrorCode = SharedConstants.ERR_CODE;
        }

        public void SetError(Exception ex)
        {
            Resultado = ResultadoEnum.Error;
            Message = ex.Message;
            MessageTrace = ex.StackTrace;
            ErrorCode = SharedConstants.ERR_CODE;
        }

        public void SetError(string message, string messageTrace, string errorCode)
        {
            Resultado = ResultadoEnum.Error;
            Message = message;
            MessageTrace = messageTrace;
            ErrorCode = errorCode;
        }

        public void SetErrorConCodigo(string message, string pCodigoError)
        {
            Resultado = ResultadoEnum.Error;
            Message = message;
            ErrorCode = pCodigoError;
        }
    }
}
