﻿using System;

namespace Shared.DTO
{
    public class ParameterDTO
    {
        public string Nombre { get; set; }
        public Object Valor { get; set; }
    }
}
