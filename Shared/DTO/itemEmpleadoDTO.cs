﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTO
{
    public class itemEmpleadoDTO
    {
        public decimal IdEmpleado { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string PrimerNombre { get; set; }
        public string OtrosNombres { get; set; }
        public string Pais { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string CorreoElectronico { get; set; }
        public string FechaIngreso { get; set; }
        public decimal IdArea { get; set; }
        public string Estado { get; set; }
        public string FechaHoraRegistro { get; set; }
        public string FechaHoraEdicion { get; set; }

        public string NombrePais { get; set; }
        public string NombreEstado { get; set; }
        public string NombreTipoIdentificacion { get; set; }
        public string NombreArea { get; set; }
    }
}
