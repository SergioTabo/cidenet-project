﻿
namespace Shared.DTO
{
    public class CredencialesDTO
    {
        public string Host { get; set; }
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public string Puerto { get; set; }
        public string ServiceName { get; set; }
        public string BaseDeDatos { get; set; }
    }
}
