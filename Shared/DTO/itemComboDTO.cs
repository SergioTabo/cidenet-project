﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTO
{
    public class itemComboDTO
    {
        public string Valor { get; set; } //por defecto se deja string
        public string Nombre { get; set; }
    }
}
