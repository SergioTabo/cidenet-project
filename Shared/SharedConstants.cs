﻿namespace Shared
{
    public enum ResultadoEnum
    {
        Ok = 0,
        Error = 1
    }

    public class SEPARADOR
    {
        public const string MILES = ".";
        public const string DECIMALES = ",";
        public const char MILES_CHAR = '.';
        public const char DECIMALES_CHAR = ',';
    }

    public class SeparadorFecha
    {
        public const string Visual = "/";
        public const string BaseDatos = "";
    }

    public class Paises
    {
        public const string Colombia = "CO";
        public const string EstadosUnidos = "US";
    }

    public class Dominios
    {
        public const string Colombia = "co";
        public const string EstadosUnidos = "us";
    }

    public class SeparadorHora
    {
        public const string Visual = ":";
    }

    public class SharedConstants
    {
        public const string OK = "OK";

        public const string ERR_CODE_EXCEPCION = "000";
        public const string ERR_CODE = "001";
        public const string ERR_CODE_ENTIDAD_NO_PUEDE_ELIMINARSE = "N100";

    }

    public class SimboloMoneda
    {
        public const string Peso = "$";
    }
}