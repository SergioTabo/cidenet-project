﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;


namespace Shared
{
    public class SharedFunciones
    {
        static string SEPARADOR_DECIMAL_REGIONAL = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

        public static string DecimalAStringConFormatos(decimal pDato)
        {
            object vObjeto = Convert.ChangeType(pDato, Type.GetType("System.String"));
            string vCadena = vObjeto.ToString() + "";
            vCadena = vCadena.Replace(SEPARADOR_DECIMAL_REGIONAL, SEPARADOR.DECIMALES);
            return vCadena;
        }

        public static string DecimalAMoneda(decimal pDato)
        {
            string vDummy = DecimalAStringConFormatos(pDato);
            return SimboloMoneda.Peso + " " + vDummy;
        }



        public static string aString(dynamic pDato)
        {
            if (pDato == null)
            {
                pDato = "";
            }

            object vObjeto = Convert.ChangeType(pDato, Type.GetType("System.String"));
            return vObjeto.ToString() + "";
        }


        public static DateTime getFechaActual()
        {
            return DateTime.Now;
        }



        public static decimal aDecimal(dynamic pDato)
        {
            string vCadena = aString(pDato);

            // Quitar los separadores de miles

            vCadena = vCadena.Replace(SEPARADOR.MILES, "");
            vCadena = vCadena.Replace(SEPARADOR.DECIMALES, SEPARADOR_DECIMAL_REGIONAL);

            decimal vDumm = decimal.Parse(vCadena);
            return vDumm;
        }

        public static decimal MonedaADecimal(dynamic pDato)
        {
            string vCadena = aString(pDato);
            vCadena = vCadena.Replace(SimboloMoneda.Peso, "");            
            return aDecimal(vCadena);
        }

        public static string fechaCortaBDAVisualizacion(string vFechaBaseDatos)
        {

            if (vFechaBaseDatos == null) return "";
            if (vFechaBaseDatos.Equals("")) return "";
            string vAno, vMes, vDia;
            vAno = vFechaBaseDatos.Substring(4, 4);
            vMes = vFechaBaseDatos.Substring(2, 2);
            vDia = vFechaBaseDatos.Substring(0, 2);
            return vDia + SeparadorFecha.Visual + vMes + SeparadorFecha.Visual + vAno;

        }

        public static string fechaCortaVisualizacionABD(string vFechaVisualizacion)
        {
            //if (vFechaVisualizacion.Equals("")) return "";
            //string vAno, vMes, vDia;
            //vAno = vFechaVisualizacion.Substring(0, 4);
            //vMes = vFechaVisualizacion.Substring(4, 2);
            //vDia = vFechaVisualizacion.Substring(6, 2);
            //return vDia + SeparadorFecha.Visual + vMes + SeparadorFecha.Visual + vAno;

            string[] vARRFec;
            string[] vPartesFecha;
            string vTiempo = "";

            if (vFechaVisualizacion == null || vFechaVisualizacion.Equals(""))
            {
                return "";
            }

            //Valida que venga solo fecha
            if (vFechaVisualizacion.IndexOf(" ") == -1)
            {
                vARRFec = vFechaVisualizacion.Split('/');
            }
            else
            {
                vPartesFecha = vFechaVisualizacion.Split(' ');
                vARRFec = vPartesFecha[0].Split('/');
                vTiempo = vPartesFecha[1].Replace(":", "");
            }

            string vAno, vMes, vDia;
            //vAno = vFechaVisualizacion.Substring(0, 4);
            //vMes = vFechaVisualizacion.Substring(4, 2);
            //vDia = vFechaVisualizacion.Substring(6, 2);
            vAno = vARRFec[2];
            vMes = vARRFec[1].PadLeft(2, '0');
            vDia = vARRFec[0].PadLeft(2, '0');
            return vDia + vMes + vAno + vTiempo;

        }

        public static string dateTimeToString(DateTime pFecha)
        {
            return pFecha.ToString("yyyyMMddHHmmss");
        }

        public static string getFechaActualString()
        {
            return dateTimeToString(getFechaActual());
        }

        public static string dateToString(DateTime pFecha)
        {
            return pFecha.ToString("yyyyMMdd");
        }

        public static string getFechaCortaActualString()
        {
            return dateToString(getFechaActual());
        }

        public static string decimalToString(decimal pValor)
        {
            string vValorTMP = pValor.ToString();
            vValorTMP = vValorTMP.Replace(',', '.');
            return vValorTMP;
        }

        public static string decimalToIntString(decimal pValor)
        {
            string vValorTMP = pValor.ToString();
            vValorTMP = vValorTMP.Replace(',', '.');
            vValorTMP = vValorTMP.Replace('.', '#');
            if (vValorTMP.IndexOf('#') <= 0)
            {
                return pValor.ToString();
            }
            vValorTMP = vValorTMP.Substring(0, vValorTMP.IndexOf('#'));
            return vValorTMP;
        }

        public static string getCadenaCompleta(string fecha)
        {
            try
            {
                string dia = fecha.Substring(0, 2);
                string mes = fecha.Substring(3, 2);
                string ano = fecha.Substring(6, 4);
                return (ano + mes + dia + "000000");
            }
            catch
            { return ""; }
        }

        public static DateTime getDateTime(string fechaCompleta)
        {
            int ano = Int32.Parse(fechaCompleta.Substring(0, 4));
            int mes = Int32.Parse(fechaCompleta.Substring(4, 2));
            int dia = Int32.Parse(fechaCompleta.Substring(6, 2));
            int hora = Int32.Parse(fechaCompleta.Substring(8, 2));
            int minutos = Int32.Parse(fechaCompleta.Substring(10, 2));
            int segundos = Int32.Parse(fechaCompleta.Substring(12, 2));

            return (new DateTime(ano, mes, dia, hora, minutos, segundos));
        }

        public static string fechaHoraCortaBDAVisualizacion(string vFechaBaseDatos)
        {
            if (vFechaBaseDatos.Equals("")) return "";
            string vAno, vMes, vDia, vHora, vMin, vSeg;
            vAno = vFechaBaseDatos.Substring(0, 4);
            vMes = vFechaBaseDatos.Substring(4, 2);
            vDia = vFechaBaseDatos.Substring(6, 2);
            vHora = vFechaBaseDatos.Substring(8, 2);
            vMin = vFechaBaseDatos.Substring(10, 2);
            vSeg = vFechaBaseDatos.Substring(12, 2);
            return vDia + SeparadorFecha.Visual + vMes + SeparadorFecha.Visual + vAno + ' ' + vHora + SeparadorHora.Visual + vMin + SeparadorHora.Visual + vSeg;

        }

        public static DateTime getDate(string fechaCompleta)
        {
            int ano = Int32.Parse(fechaCompleta.Substring(0, 4));
            int mes = Int32.Parse(fechaCompleta.Substring(4, 2));
            int dia = Int32.Parse(fechaCompleta.Substring(6, 2));

            return (new DateTime(ano, mes, dia));
        }

        public static string getFechaCortaActualEscrita()
        {
            string Fecha = getFechaCortaActualString();
            return getFechaCortaEscrita(Fecha);
        }

        public static string getFechaCortaEscrita(string Fecha)
        {
            string retorno;
            retorno = Fecha.Substring(6) + " de " + NombreMes(int.Parse(Fecha.Substring(4, 2))) + " de " + Fecha.Substring(0, 4);
            return retorno;
        }

        public static string NombreMes(int mes)
        {
            switch (mes)
            {
                case 1:
                    {
                        return "Enero";
                    }
                case 2:
                    {
                        return "Febrero";
                    }
                case 3:
                    {
                        return "Marzo";
                    }
                case 4:
                    {
                        return "Abril";
                    }
                case 5:
                    {
                        return "Mayo";
                    }
                case 6:
                    {
                        return "Junio";
                    }
                case 7:
                    {
                        return "Julio";
                    }
                case 8:
                    {
                        return "Agosto";
                    }
                case 9:
                    {
                        return "Septiembre";
                    }
                case 10:
                    {
                        return "Octubre";
                    }
                case 11:
                    {
                        return "Noviembre";
                    }
                case 12:
                    {
                        return "Diciembre";
                    }
                default:
                    {
                        return mes.ToString();
                    }

            }
        }

        public static string NombreMesCorto(int mes)
        {
            switch (mes)
            {
                case 1:
                    {
                        return "Ene";
                    }
                case 2:
                    {
                        return "Feb";
                    }
                case 3:
                    {
                        return "Mar";
                    }
                case 4:
                    {
                        return "Abr";
                    }
                case 5:
                    {
                        return "May";
                    }
                case 6:
                    {
                        return "Jun";
                    }
                case 7:
                    {
                        return "Jul";
                    }
                case 8:
                    {
                        return "Ago";
                    }
                case 9:
                    {
                        return "Sep";
                    }
                case 10:
                    {
                        return "Oct";
                    }
                case 11:
                    {
                        return "Nov";
                    }
                case 12:
                    {
                        return "Dic";
                    }
                default:
                    {
                        return mes.ToString();
                    }

            }
        }

        public static bool EsNumero(string Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Expression, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        public static String ponerFormato(String numero)
        {
            if (numero == null)
                return "0";
            String dummy = numero;
            String parte_decimal;
            String parte_entera;
            String p_entera_formateada;
            int j;
            int pos_decimal = dummy.IndexOf(SEPARADOR.DECIMALES);

            if (pos_decimal == -1)
            {
                parte_entera = dummy;
                parte_decimal = "";
            }
            else
            {
                parte_entera = dummy.Substring(0, pos_decimal);
                parte_decimal = dummy.Substring(pos_decimal + 1);
            }

            p_entera_formateada = "";
            j = 0;

            for (int i = parte_entera.Length; i > 0; i--)
            {
                if (j != 3)
                {
                    p_entera_formateada = parte_entera.Substring(i - 1, 1) + p_entera_formateada;
                    j++;
                }
                if (j == 3 && i != 1)
                {
                    p_entera_formateada = SEPARADOR.MILES + p_entera_formateada;
                    j = 0;
                }
            }

            dummy = p_entera_formateada + (parte_decimal.Equals("") ? "" : (SEPARADOR.DECIMALES + parte_decimal));
            return dummy;
        }

        public static string getFechaActualCompletaFormatoBD()
        {
            return DateTime.Now.Year.ToString() +
                   DateTime.Now.Month.ToString().PadLeft(2, '0') +
                   DateTime.Now.Day.ToString().PadLeft(2, '0') +
                   DateTime.Now.Hour.ToString().PadLeft(2, '0') +
                   DateTime.Now.Minute.ToString().PadLeft(2, '0') +
                   DateTime.Now.Second.ToString().PadLeft(2, '0');
        }
    }
}
