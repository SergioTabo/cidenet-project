﻿using DataAccess.Conexiones.Contratos;
using DataAccess.Core.Contratos;
using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;


namespace DataAccess.Core.Implementacion
{
    public class DAL_Empleado : MDAL_Empleado, IDAL_Empleado
    {
        public DAL_Empleado(IConexion conexion)
        {
            this._Conexion = conexion;
        }

        public DataTable getEmpleadosxFiltro(string pFiltro, decimal pItemsPorPagina, decimal pPaginaActual)
        {
            try
            {
                IList<ParameterDTO> vParametros = new List<ParameterDTO>();
                string vFiltroSQL = "";
                string vPaginador = "";

                decimal vLimite = (pPaginaActual * pItemsPorPagina) - pItemsPorPagina;

                vPaginador = @"offset :P_PAGINA_ACTUAL rows
                               fetch next :P_ITEMS rows only ";
                vParametros = new List<ParameterDTO>() {
                new ParameterDTO(){ Nombre = ":P_PAGINA_ACTUAL", Valor = int.Parse(vLimite.ToString()) },
                new ParameterDTO(){ Nombre = ":P_ITEMS", Valor = int.Parse(pItemsPorPagina.ToString()) }
                };

                if (pFiltro != null && !pFiltro.Equals(""))
                {
                    vFiltroSQL += " and (upper( E.PRIMER_APELLIDO + ' ' + E.SEGUNDO_APELLIDO + ' ' + E.PRIMER_NOMBRE + ' ' + E.OTROS_NOMBRES + ' ' + TI.NOMBRE + ' ' +  E.NUMERO_IDENTIFICACION + ' ' + P.NOMBRE + ' ' + ES.NOMBRE + ' ' + E.CORREO_ELECTRONICO) LIKE '%" + pFiltro.ToString().ToUpper() + "%') ";
                }

                DataTable dt = new DataTable();
                string sql = @"select E.ID_EMPLEADO,
                                        E.PRIMER_APELLIDO,
                                        E.SEGUNDO_APELLIDO,
                                        E.PRIMER_NOMBRE,
                                        E.OTROS_NOMBRES,
                                        E.PAIS,
                                        E.TIPO_IDENTIFICACION,
                                        E.NUMERO_IDENTIFICACION,
                                        E.CORREO_ELECTRONICO,
                                        E.ID_AREA,
                                        E.ESTADO,
                                        ES.NOMBRE,
                                        TI.NOMBRE,
                                        P.NOMBRE,
                                        AR.NOMBRE
                                        from EMPLEADO E
                                        inner join PARAMETRIZACION ES on ES.CODIGO = E.ESTADO and ES.TIPO = 'E' and ES.ESTADO = 'A'
                                        inner join PARAMETRIZACION TI on TI.CODIGO = E.TIPO_IDENTIFICACION and TI.TIPO = 'TI' and TI.ESTADO = 'A'
                                        inner join PARAMETRIZACION P on P.CODIGO = E.PAIS and P.TIPO = 'P' and P.ESTADO = 'A'
                                        inner join PARAMETRIZACION AR on AR.CODIGO = E.ID_AREA and AR.TIPO = 'A' and ES.ESTADO = 'A'"
                                        + vFiltroSQL;

                sql += " order by E.ID_EMPLEADO DESC ";

                sql += vPaginador;

                dt = this._Conexion.Consultar(sql, vParametros);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow getEmpleadoxIdentificacion(string pIdentificacion, string pTipoIdentificacion)
        {
            try
            {
                DataTable dt = new DataTable();
                string sql = @"select ID_EMPLEADO,
                                        PRIMER_APELLIDO,
                                        SEGUNDO_APELLIDO,
                                        PRIMER_NOMBRE,
                                        OTROS_NOMBRES,
                                        PAIS,
                                        TIPO_IDENTIFICACION,
                                        NUMERO_IDENTIFICACION,
                                        CORREO_ELECTRONICO,
                                        FECHA_INGRESO,
                                        ID_AREA,
                                        ESTADO,
                                        FECHA_HORA_REGISTRO,
                                        FECHA_HORA_EDICION
                                        from EMPLEADO
                                        where NUMERO_IDENTIFICACION = :P_NUMERO_IDENTIFICACION
                                        and TIPO_IDENTIFICACION = :P_TIPO_IDENTIFICACION";
                IList<ParameterDTO> vParametros = new List<ParameterDTO>() {
                new ParameterDTO(){ Nombre = ":P_NUMERO_IDENTIFICACION", Valor = pIdentificacion },
                new ParameterDTO(){ Nombre = ":P_TIPO_IDENTIFICACION", Valor = pTipoIdentificacion }
                };
                dt = this._Conexion.Consultar(sql, vParametros);
                return dt.Rows[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow getEmpleadoxCorreoElectronico(string pCorreo)
        {
            try
            {
                DataTable dt = new DataTable();
                string sql = @"select ID_EMPLEADO,
                                        PRIMER_APELLIDO,
                                        SEGUNDO_APELLIDO,
                                        PRIMER_NOMBRE,
                                        OTROS_NOMBRES,
                                        PAIS,
                                        TIPO_IDENTIFICACION,
                                        NUMERO_IDENTIFICACION,
                                        CORREO_ELECTRONICO,
                                        FECHA_INGRESO,
                                        ID_AREA,
                                        ESTADO,
                                        FECHA_HORA_REGISTRO,
                                        FECHA_HORA_EDICION
                                        from EMPLEADO
                                        where CORREO_ELECTRONICO = :P_CORREO_ELECTRONICO ";
                IList<ParameterDTO> vParametros = new List<ParameterDTO>() {
                new ParameterDTO(){ Nombre = ":P_CORREO_ELECTRONICO", Valor = pCorreo }
                };
                dt = this._Conexion.Consultar(sql, vParametros);
                return dt.Rows[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow getTotalElementos(string pFiltro)
        {
            try
            {
                IList<ParameterDTO> vParametros = new List<ParameterDTO>();
                string vFiltroSQL = "";

                if (pFiltro != null && !pFiltro.Equals(""))
                {
                    vFiltroSQL += " and (upper( E.PRIMER_APELLIDO + ' ' + E.SEGUNDO_APELLIDO + ' ' + E.PRIMER_NOMBRE + ' ' + E.OTROS_NOMBRES + ' ' + TI.NOMBRE + ' ' +  E.NUMERO_IDENTIFICACION + ' ' + P.NOMBRE + ' ' + ES.NOMBRE + ' ' + E.CORREO_ELECTRONICO) LIKE '%" + pFiltro.ToString().ToUpper() + "%') ";
                }

                DataTable dt = new DataTable();
                string sql = @"select COUNT(*)
                                        from EMPLEADO E
                                        inner join PARAMETRIZACION ES on ES.CODIGO = E.ESTADO and ES.TIPO = 'E' and ES.ESTADO = 'A'
                                        inner join PARAMETRIZACION TI on TI.CODIGO = E.TIPO_IDENTIFICACION and TI.TIPO = 'TI' and TI.ESTADO = 'A'
                                        inner join PARAMETRIZACION P on P.CODIGO = E.PAIS and P.TIPO = 'P' and P.ESTADO = 'A'
                                        inner join PARAMETRIZACION AR on AR.CODIGO = E.ID_AREA and AR.TIPO = 'A' and ES.ESTADO = 'A'"
                                        + vFiltroSQL;

                dt = this._Conexion.Consultar(sql, vParametros);
                return dt.Rows[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}