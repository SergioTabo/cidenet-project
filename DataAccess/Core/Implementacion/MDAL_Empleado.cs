﻿using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccess.Core.Implementacion
{
    public class MDAL_Empleado : META_DAL
    {

        public DataRow getEntidadXUid(decimal pUid)
        {
            try
            {
                DataTable dt = new DataTable();
                string sql = @"select ID_EMPLEADO,
PRIMER_APELLIDO,
SEGUNDO_APELLIDO,
PRIMER_NOMBRE,
OTROS_NOMBRES,
PAIS,
TIPO_IDENTIFICACION,
NUMERO_IDENTIFICACION,
CORREO_ELECTRONICO,
FECHA_INGRESO,
ID_AREA,
ESTADO,
FECHA_HORA_REGISTRO,
FECHA_HORA_EDICION
from EMPLEADO
where ID_EMPLEADO = :P_ID_EMPLEADO ";
                IList<ParameterDTO> vParametros = new List<ParameterDTO>() {
new ParameterDTO(){ Nombre = ":P_ID_EMPLEADO", Valor = pUid }
};
                dt = this._Conexion.Consultar(sql, vParametros);
                return dt.Rows[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListaEmpleado()
        {
            try
            {
                DataTable dt = new DataTable();
                string sql = @"select ID_EMPLEADO,
PRIMER_APELLIDO,
SEGUNDO_APELLIDO,
PRIMER_NOMBRE,
OTROS_NOMBRES,
PAIS,
TIPO_IDENTIFICACION,
NUMERO_IDENTIFICACION,
CORREO_ELECTRONICO,
FECHA_INGRESO,
ID_AREA,
ESTADO,
FECHA_HORA_REGISTRO,
FECHA_HORA_EDICION
from EMPLEADO
ORDER BY ";

                dt = this._Conexion.Consultar(sql);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal insert(decimal pIdEmpleado, string pPrimerApellido, string pSegundoApellido, string pPrimerNombre, string pOtrosNombres, string pPais, string pTipoIdentificacion, string pNumeroIdentificacion, string pCorreoElectronico, string pFechaIngreso, decimal pIdArea, string pEstado, string pFechaHoraRegistro, string pFechaHoraEdicion)
        {
            try
            {
                IList<ParameterDTO> vParametros = new List<ParameterDTO>();
                vParametros.Add(new ParameterDTO() { Nombre = ":P_PRIMER_APELLIDO", Valor = pPrimerApellido });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_SEGUNDO_APELLIDO", Valor = pSegundoApellido });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_PRIMER_NOMBRE", Valor = pPrimerNombre });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_OTROS_NOMBRES", Valor = pOtrosNombres });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_PAIS", Valor = pPais });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_TIPO_IDENTIFICACION", Valor = pTipoIdentificacion });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_NUMERO_IDENTIFICACION", Valor = pNumeroIdentificacion });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_CORREO_ELECTRONICO", Valor = pCorreoElectronico });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_FECHA_INGRESO", Valor = pFechaIngreso });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_ID_AREA", Valor = pIdArea });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_ESTADO", Valor = pEstado });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_FECHA_HORA_REGISTRO", Valor = pFechaHoraRegistro });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_FECHA_HORA_EDICION", Valor = pFechaHoraEdicion });

                String sql = @"insert into
EMPLEADO (
PRIMER_APELLIDO,
SEGUNDO_APELLIDO,
PRIMER_NOMBRE,
OTROS_NOMBRES,
PAIS,
TIPO_IDENTIFICACION,
NUMERO_IDENTIFICACION,
CORREO_ELECTRONICO,
FECHA_INGRESO,
ID_AREA,
ESTADO,
FECHA_HORA_REGISTRO,
FECHA_HORA_EDICION
) values (
:P_PRIMER_APELLIDO,
:P_SEGUNDO_APELLIDO,
:P_PRIMER_NOMBRE,
:P_OTROS_NOMBRES,
:P_PAIS,
:P_TIPO_IDENTIFICACION,
:P_NUMERO_IDENTIFICACION,
:P_CORREO_ELECTRONICO,
:P_FECHA_INGRESO,
:P_ID_AREA,
:P_ESTADO,
:P_FECHA_HORA_REGISTRO,
:P_FECHA_HORA_EDICION
) RETURNING ID_EMPLEADO INTO :RETORNO";
                return this._Conexion.Insert(sql, vParametros);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void update(decimal pIdEmpleado, string pPrimerApellido, string pSegundoApellido, string pPrimerNombre, string pOtrosNombres, string pPais, string pTipoIdentificacion, string pNumeroIdentificacion, string pCorreoElectronico, string pFechaIngreso, decimal pIdArea, string pEstado, string pFechaHoraRegistro, string pFechaHoraEdicion)
        {
            try
            {
                IList<ParameterDTO> vParametros = new List<ParameterDTO>();
                vParametros.Add(new ParameterDTO() { Nombre = ":P_PRIMER_APELLIDO", Valor = pPrimerApellido });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_SEGUNDO_APELLIDO", Valor = pSegundoApellido });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_PRIMER_NOMBRE", Valor = pPrimerNombre });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_OTROS_NOMBRES", Valor = pOtrosNombres });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_PAIS", Valor = pPais });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_TIPO_IDENTIFICACION", Valor = pTipoIdentificacion });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_NUMERO_IDENTIFICACION", Valor = pNumeroIdentificacion });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_CORREO_ELECTRONICO", Valor = pCorreoElectronico });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_FECHA_INGRESO", Valor = pFechaIngreso });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_ID_AREA", Valor = pIdArea });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_ESTADO", Valor = pEstado });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_FECHA_HORA_REGISTRO", Valor = pFechaHoraRegistro });
                vParametros.Add(new ParameterDTO() { Nombre = ":P_FECHA_HORA_EDICION", Valor = pFechaHoraEdicion });

                vParametros.Add(new ParameterDTO() { Nombre = ":P_ID_EMPLEADO", Valor = pIdEmpleado });

                String sql = @"UPDATE EMPLEADO SET
PRIMER_APELLIDO = :P_PRIMER_APELLIDO ,
SEGUNDO_APELLIDO = :P_SEGUNDO_APELLIDO ,
PRIMER_NOMBRE = :P_PRIMER_NOMBRE ,
OTROS_NOMBRES = :P_OTROS_NOMBRES ,
PAIS = :P_PAIS ,
TIPO_IDENTIFICACION = :P_TIPO_IDENTIFICACION ,
NUMERO_IDENTIFICACION = :P_NUMERO_IDENTIFICACION ,
CORREO_ELECTRONICO = :P_CORREO_ELECTRONICO ,
FECHA_INGRESO = :P_FECHA_INGRESO ,
ID_AREA = :P_ID_AREA ,
ESTADO = :P_ESTADO ,
FECHA_HORA_REGISTRO = :P_FECHA_HORA_REGISTRO ,
FECHA_HORA_EDICION = :P_FECHA_HORA_EDICION
WHERE ID_EMPLEADO = :P_ID_EMPLEADO";
                this._Conexion.Ejecutar(sql, vParametros);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(decimal pIdEmpleado)
        {
            try
            {
                IList<ParameterDTO> vParametros = new List<ParameterDTO>() {
new ParameterDTO(){ Nombre = ":P_ID_EMPLEADO", Valor = pIdEmpleado }

};
                String sql = @"DELETE EMPLEADO
WHERE ID_EMPLEADO = :P_ID_EMPLEADO";
                this._Conexion.Ejecutar(sql, vParametros);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}