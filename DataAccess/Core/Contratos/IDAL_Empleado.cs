﻿using System;
using System.Data;

namespace DataAccess.Core.Contratos
{
    public interface IDAL_Empleado
    {
        DataTable ListaEmpleado();
        decimal insert(decimal pIdEmpleado, string pPrimerApellido, string pSegundoApellido, string pPrimerNombre, string pOtrosNombres, string pPais, string pTipoIdentificacion, string pNumeroIdentificacion, string pCorreoElectronico, string pFechaIngreso, decimal pIdArea, string pEstado, string pFechaHoraRegistro, string pFechaHoraEdicion);
        void update(decimal pIdEmpleado, string pPrimerApellido, string pSegundoApellido, string pPrimerNombre, string pOtrosNombres, string pPais, string pTipoIdentificacion, string pNumeroIdentificacion, string pCorreoElectronico, string pFechaIngreso, decimal pIdArea, string pEstado, string pFechaHoraRegistro, string pFechaHoraEdicion);
        void delete(decimal pIdEmpleado);
        DataRow getEntidadXUid(decimal pUid);
    }
}