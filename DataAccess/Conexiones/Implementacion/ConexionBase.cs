﻿using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Conexiones.Implementacion
{
    public abstract class ConexionBase
    {

        protected CredencialesDTO Credenciales;
        public string TipoConexion;

        public abstract void Conectar();
        public abstract bool CerrarConexion();
        public abstract DataTable Consultar(string sql);
        public abstract DataTable Consultar(string sql, IList<ParameterDTO> Parametros);
        public abstract decimal Insert(string sql, IList<ParameterDTO> Parametros);
        public abstract void Ejecutar(string sql, IList<ParameterDTO> Parametros);

        public abstract void SubirFimra(decimal pUidUsuario, decimal pUidUsuarioModifica, byte[] pDatos);
        public abstract Byte[] consultarFirma(decimal pUidUsuario);

        public abstract void actualizarBlob(decimal pLlave, string pCampoLlave, string pTabla, string pCampoBlob, byte[] pDatos);
        public abstract Byte[] consultarBlob(decimal pLlave, string pTabla, string pCampoLlave, string pCampoBlob);

        #region TRANSACCIONES
        public abstract void BeginTransaction();
        public abstract void CommitTransaction();
        public abstract void RollBackTransaction();
        public abstract bool getIsBeginTransaction();
        #endregion TRANSACCIONES
    }
}
