﻿using DataAccess.Conexiones.Contratos;
using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Conexiones.Implementacion
{
    public class ConexionSqlServer : ConexionBase, IConexion
    {
        private SqlConnection conexion;
        private SqlTransaction transaction;
        public bool IsBeginTransaction = false;

        public ConexionSqlServer(CredencialesDTO Credenciales)
        {
            this.Credenciales = Credenciales;
        }

        public override void Conectar()
        {
            try
            {
                this.conexion = new SqlConnection();
                this.conexion.ConnectionString = "Server = " + this.Credenciales.Host + "; Database = " + this.Credenciales.BaseDeDatos + "; User Id = " + this.Credenciales.Usuario + "; Password = " + this.Credenciales.Clave + ";";
                //this.conexion.ConnectionString = "Server = " + this.Credenciales.Host + "; Database = " + this.Credenciales.BaseDeDatos + "; Integrated Security=true;";

                if (this.conexion.State.Equals(ConnectionState.Closed))
                {
                    this.conexion.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override bool CerrarConexion()
        {
            try
            {
                this.conexion.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override DataTable Consultar(string sql)
        {
            DataTable datable = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                SqlDataAdapter oda = new SqlDataAdapter(cmd);
                oda.Fill(datable);
                return datable;
            }
            catch (SqlException oe)
            {
                throw new Exception(oe.Message, oe.InnerException);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Replace('\'', '\"'));
            }

        }

        public override DataTable Consultar(string sql, IList<ParameterDTO> Parametros)
        {
            DataTable datable = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(sql.Replace(':', '@'));
                foreach (ParameterDTO item in Parametros)
                {
                    cmd.Parameters.Add(new SqlParameter(item.Nombre.Replace(':', '@'), item.Valor));
                }
                cmd.Transaction = this.transaction;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                SqlDataAdapter oda = new SqlDataAdapter(cmd);
                oda.Fill(datable);
                return datable;
            }
            catch (SqlException oe)
            {
                throw new Exception(oe.Message, oe.InnerException);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Replace('\'', '\"'));
            }
        }

        public override decimal Insert(string sql, IList<ParameterDTO> Parametros)
        {
            DataTable datable = new DataTable();
            try
            {
                String[] spearator = { "RETURNING" };
                string[] vPartesSql = sql.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                string vSql = vPartesSql[0].Replace(':', '@');
                string[] vPartesId = (vPartesSql[1]).TrimStart().Split(' ');
                SqlCommand cmd = new SqlCommand(vSql.Replace("values", " OUTPUT INSERTED." + vPartesId[0] + " values "));
                foreach (ParameterDTO item in Parametros)
                {
                    cmd.Parameters.Add(new SqlParameter(item.Nombre.Replace(':', '@'), item.Valor));
                }

                cmd.Transaction = this.transaction;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                Int32 vUid = (Int32)cmd.ExecuteScalar();
                //return vUid;
                return decimal.Parse(vUid.ToString());
            }
            catch (SqlException oe)
            {
                throw new Exception(oe.Message, oe.InnerException);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Replace('\'', '\"'));
            }
        }

        public override void Ejecutar(string sql, IList<ParameterDTO> Parametros)
        {
            DataTable datable = new DataTable();
            try
            {

                SqlCommand cmd = new SqlCommand(sql.Replace(':', '@'));

                if (Parametros != null)
                {
                    foreach (ParameterDTO item in Parametros)
                    {
                        cmd.Parameters.Add(new SqlParameter(item.Nombre.Replace(':', '@'), item.Valor));
                    }
                }

                cmd.Transaction = this.transaction;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException oe)
            {
                throw new Exception(oe.Message, oe.InnerException);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Replace('\'', '\"'));
            }
        }

        public override void SubirFimra(decimal pUidUsuario, decimal pUidUsuarioModifica, byte[] pDatos)
        {
            DataTable datable = new DataTable();
            try
            {
                string sql = "delete from SEAM_FIRMA_USUARIO where UID_USUARIO = " + pUidUsuario;
                this.Ejecutar(sql, new List<ParameterDTO>());

                sql = "insert into SEAM_FIRMA_USUARIO (UID_USUARIO, FIRMA, UID_USUARIO_MODIFICA) values (:1, :2, :3)";
                SqlCommand cmd = new SqlCommand(sql);

                cmd.Parameters.Add(new SqlParameter(":1", pUidUsuario));
                SqlParameter vParamBlob = new SqlParameter(":2", SqlDbType.Binary);
                vParamBlob.Value = pDatos;
                cmd.Parameters.Add(vParamBlob);
                cmd.Parameters.Add(new SqlParameter(":3", pUidUsuarioModifica));


                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException oe)
            {
                throw new Exception(oe.Message, oe.InnerException);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Replace('\'', '\"'));
            }
        }

        public override void actualizarBlob(decimal pLlave, string pCampoLlave, string pTabla, string pCampoBlob, byte[] pDatos)
        {
            DataTable datable = new DataTable();
            try
            {

                string sql = "update " + pTabla + " set " + pCampoBlob + " = :1 where " + pCampoLlave + " = :2";
                SqlCommand cmd = new SqlCommand(sql);

                SqlParameter vParamBlob = new SqlParameter(":1", SqlDbType.Binary);
                vParamBlob.Value = pDatos;
                cmd.Parameters.Add(vParamBlob);
                cmd.Parameters.Add(new SqlParameter(":2", pLlave));

                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException oe)
            {
                throw new Exception(oe.Message, oe.InnerException);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Replace('\'', '\"'));
            }
        }

        public override Byte[] consultarFirma(decimal pUidUsuario)
        {
            Byte[] blob = null;
            try
            {
                string sql = "select nvl(firma, (select firma from SEAM_FIRMA_NULA) ) from SEAM_FIRMA_USUARIO where UID_USUARIO = " + pUidUsuario;

                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                SqlDataReader vdr = cmd.ExecuteReader();
                vdr.Read();
                blob = new Byte[vdr.GetBytes(0, 0, null, 0, int.MaxValue)];
                vdr.GetBytes(0, 0, blob, 0, blob.Length);
                vdr.Close();

                return blob;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override Byte[] consultarBlob(decimal pLlave, string pTabla, string pCampoLlave, string pCampoBlob)
        {
            Byte[] blob = null;
            try
            {
                string sql = "select " + pCampoBlob + " from " + pTabla + " where " + pCampoLlave + " = " + pLlave;

                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = this.conexion;
                SqlDataReader vdr = cmd.ExecuteReader();
                vdr.Read();
                blob = new Byte[vdr.GetBytes(0, 0, null, 0, int.MaxValue)];
                vdr.GetBytes(0, 0, blob, 0, blob.Length);
                vdr.Close();

                return blob;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region TRANSACCIONES
        public override void BeginTransaction()
        {
            this.transaction = this.conexion.BeginTransaction();
            this.IsBeginTransaction = true;
        }

        public override void CommitTransaction()
        {
            this.transaction.Commit();
            this.IsBeginTransaction = false;
        }

        public override void RollBackTransaction()
        {
            this.transaction.Rollback();
            this.IsBeginTransaction = false;
        }

        public override bool getIsBeginTransaction()
        {
            return this.IsBeginTransaction;
        }
        #endregion TRANSACCIONES        
    }
}
