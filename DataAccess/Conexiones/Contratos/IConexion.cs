﻿using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Conexiones.Contratos
{
    public interface IConexion
    {
        void Conectar();
        bool CerrarConexion();
        DataTable Consultar(string sql);
        DataTable Consultar(string sql, IList<ParameterDTO> Parametros);
        decimal Insert(string sql, IList<ParameterDTO> Parametros);
        void Ejecutar(string sql, IList<ParameterDTO> Parametros);
        void SubirFimra(decimal pUidUsuario, decimal pUidUsuarioModifica, byte[] pDatos);
        Byte[] consultarFirma(decimal pUidUsuario);

        void actualizarBlob(decimal pLlave, string pCampoLlave, string pTabla, string pCampoBlob, byte[] pDatos);
        Byte[] consultarBlob(decimal pLlave, string pTabla, string pCampoLlave, string pCampoBlob);

        #region TRANSACCIONES
        void BeginTransaction();
        void CommitTransaction();
        void RollBackTransaction();
        bool getIsBeginTransaction();
        #endregion TRANSACCIONES
    }
}
