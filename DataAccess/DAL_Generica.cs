﻿using DataAccess.Conexiones.Contratos;
using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DAL_Generica : META_DAL
    {
        public DAL_Generica(IConexion conexion)
        {
            this._Conexion = conexion;
        }

        public DataTable getElementosxTipo(string pTipo)
        {
            try
            {
                DataTable dt = new DataTable();
                string sql = @"select CODIGO,
                                    NOMBRE
                                    from PARAMETRIZACION
                                    where TIPO = :P_TIPO AND ESTADO = 'A' 
                                    order by CODIGO asc ";
                IList<ParameterDTO> vParametros = new List<ParameterDTO>() {
                    new ParameterDTO(){ Nombre = ":P_TIPO", Valor = pTipo }
                    };
                dt = this._Conexion.Consultar(sql, vParametros);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
