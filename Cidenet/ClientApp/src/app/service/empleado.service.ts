import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  host: string;
  opcionesHttp: any;

  private url = "api/Empleado";

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.host = baseUrl;
    this.opcionesHttp = {
      params: new HttpParams(),
    };
  }

  /**
   * Servicio que obtiene los Empleados x Filtro
   * @param pFiltro
   */
  public getEmpleadosxFiltro(pFiltro: any, pItemsPorPagina: any, pPaginaActual: any): Observable<any> {
    this.opcionesHttp.params = this.opcionesHttp.params.set('pFiltro', pFiltro);
    this.opcionesHttp.params = this.opcionesHttp.params.set('pItemsPorPagina', pItemsPorPagina);
    this.opcionesHttp.params = this.opcionesHttp.params.set('pPaginaActual', pPaginaActual);
    return this.http.get<any>(this.host + this.url + "/getEmpleadosxFiltro", this.opcionesHttp).pipe(map(data => data));
  }

  /**
   * Servicio que almacena o actualiza un Empleado
   * @param pEmpleado
   */
  public guardarEmpleado(pEmpleado: any): Observable<any> {
    return this.http.post<any>(this.host + this.url + "/guardarEmpleado", pEmpleado).pipe(map(data => data));
  }

  /**
   * Servicio que obtiene un empleado x Id
   * @param pId
   */
  public getEmpleadoxId(pId: any): Observable<any> {
    this.opcionesHttp.params = this.opcionesHttp.params.set('pId', pId);
    return this.http.get<any>(this.host + this.url + "/getEmpleadoxId", this.opcionesHttp).pipe(map(data => data));
  }

  /**
   * Servicio que elimina un Empleado
   * @param empleado
   */
  public borrarEmpleado(pId: any): Observable<any> {
    this.opcionesHttp.params = this.opcionesHttp.params.set('pId', pId);
    return this.http.delete<any>(this.host + this.url + "/eliminarEmpleado", this.opcionesHttp).pipe(map((data) => data));
  }

}
