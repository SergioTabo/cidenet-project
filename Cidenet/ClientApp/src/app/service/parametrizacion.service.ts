import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ParametrizacionService {

  host: string;
  opcionesHttp: any;

  private url = "api/Parametrizacion";

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.host = baseUrl;
    this.opcionesHttp = {
      params: new HttpParams(),
    };

  }

  /**
   * Servicio que obtiene los datos parametricos x Tipo
   * @param pTipo
   */
  public getElementosxTipo(pTipo: any): Observable<any> {
    this.opcionesHttp.params = this.opcionesHttp.params.set('pTipo', pTipo);
    return this.http.get<any>(this.host + this.url + "/getElementosxTipo", this.opcionesHttp).pipe(map(data => data));
  }
}
