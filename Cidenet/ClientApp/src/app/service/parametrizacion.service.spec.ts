import { TestBed } from '@angular/core/testing';

import { ParametrizacionService } from './parametrizacion.service';

describe('ParametrizacionService', () => {
  let service: ParametrizacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParametrizacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
