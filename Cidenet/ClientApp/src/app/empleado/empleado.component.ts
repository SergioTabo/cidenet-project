import { Component, OnInit, TemplateRef } from '@angular/core';
import { EmpleadoService } from '../service/empleado.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ParametrizacionService } from '../service/parametrizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  empleados: any;
  empleado: any;
  filtro: string;
  respuesta: any;

  modalRef?: BsModalRef;
  config = {
    class: 'modal-lg',
    ignoreBackdropClick: true,
    keyboard: false
  }

  paises: any;
  areas: any;
  tipoIdentificacion: any;
  estados: any;
  hoy: any;
  mesAtras: any;
  editable: boolean;
  fechaIngresoEmpleado: any;
  bloqueado: boolean;

  currentPage = 1;
  page: number;
  itemsPerPage = 10;
  maxSize = 10;
  size: number;

  constructor(private empleadosService: EmpleadoService, private modalService: BsModalService, private parametrizacionService: ParametrizacionService) {
    this.empleado = [];
    this.filtro = "";
    this.hoy = new Date();
    this.mesAtras = new Date();
    this.mesAtras.setMonth(this.mesAtras.getMonth() - 1);
    this.editable = false;
    this.bloqueado = false;
  }

  ngOnInit(): void {
    this.getAreasParaCombo("A");
    this.getPaisesParaCombo("P");
    this.getIdentificacionesParaCombo("TI");
    this.getEstadosParaCombo("E");
    this.getEmpleadosxFiltro();
  }

  /**
   * Funcion que obtiene los empleados x Filtro
   * */
  public getEmpleadosxFiltro() {
    const filtro = this.filtro;
    this.empleadosService.getEmpleadosxFiltro(filtro, this.itemsPerPage, this.page ? this.page : this.currentPage).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.empleados = this.respuesta.retorno;
        this.size = this.respuesta.totalElementos;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que abre el modal para crear un Empleado
   * @param template
   */
  public abrirModal(template: TemplateRef<any>) {
    this.empleado = [];
    this.editable = false;
    this.modalRef = this.modalService.show(template, this.config);
  }

  /**
   * Funcion que obtiene las Areas para Combo/select
   * @param pTipo
   */
  public getAreasParaCombo(pTipo) {
    const tipo = pTipo;
    this.parametrizacionService.getElementosxTipo(tipo).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.areas = this.respuesta.retorno;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que obtiene los Paises para Combo/select
   * @param pTipo
   */
  public getPaisesParaCombo(pTipo) {
    const tipo = pTipo;
    this.parametrizacionService.getElementosxTipo(tipo).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.paises = this.respuesta.retorno;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que obtiene los Tipos de Identificacion para Combo/select
   * @param pTipo
   */
  public getIdentificacionesParaCombo(pTipo) {
    const tipo = pTipo;
    this.parametrizacionService.getElementosxTipo(tipo).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.tipoIdentificacion = this.respuesta.retorno;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que obtiene los Estados para Combo/select
   * @param pTipo
   */
  public getEstadosParaCombo(pTipo) {
    const tipo = pTipo;
    this.parametrizacionService.getElementosxTipo(tipo).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {

          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.estados = this.respuesta.retorno;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que valida que solo ingresen letras y un booleano que permite la tecla "Espacio"
   * @param e
   * @param pEspacio
   */
  public soloLetras(e, pEspacio) {
    var key = e.keyCode || e.which;
    var tecla = String.fromCharCode(key).toString();
    var letras = "ABCDEFGHIJKLMNOPQRSTUVWXZabcdefghijklmnopqrstuvwxyz";

    var especiales = [8, 13];

    if (pEspacio) {
      especiales.push(32);
    }

    var teclaEspecial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        teclaEspecial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !teclaEspecial) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: "Solo se permiten letras diferentes a Ñ."
      });
    }
  }

  /**
   * Funcion que valida que solo ingresen letras o numeros
   * @param e
   */
  public soloLetrasNumeros(e) {
    var key = e.keyCode || e.which;
    var tecla = String.fromCharCode(key).toString();
    var letras = "ABCDEFGHIJKLMNOPQRSTUVWXZabcdefghijklmnopqrstuvwxyz0123456789-";

    var especiales = [8, 13];

    var teclaEspecial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        teclaEspecial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !teclaEspecial) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: "Carecter no permitido."
      });
    }
  }

  /**
   * Funcion que almecena un Empleado nuevo
   * @param pEmpleado
   */
  public guardar(pEmpleado) {
    if (pEmpleado.primerApellido == null || pEmpleado.segundoApellido == null || pEmpleado.primerNombre == null || pEmpleado.pais == null || pEmpleado.tipoIdentificacion == null || pEmpleado.numeroIdentificacion == null || pEmpleado.fechaIngreso == null || pEmpleado.idArea == null) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: "Debe diligenciar todos los campos"
      });
      return;
    }

    var fechaIngreso = new Date(pEmpleado.fechaIngreso);
    var dia = fechaIngreso.getDate();
    var mes = fechaIngreso.getMonth() + 1;
    var anio = fechaIngreso.getFullYear();
    if (dia < 10) {
      var myDia = "0" + dia;
    } else {
      var myDia = dia.toString();
    }

    if (mes < 10) {
      var myMes = "0" + mes;
    } else {
      var myMes = mes.toString();
    }

    var fecha = myDia + "/" + myMes + "/" + anio;
    const vObj = {
      IdEmpleado: -1,
      PrimerApellido: pEmpleado.primerApellido,
      SegundoApellido: pEmpleado.segundoApellido,
      PrimerNombre: pEmpleado.primerNombre,
      OtrosNombres: pEmpleado.otrosNombres == null ? "" : pEmpleado.otrosNombres,
      Pais: pEmpleado.pais,
      TipoIdentificacion: pEmpleado.tipoIdentificacion,
      NumeroIdentificacion: pEmpleado.numeroIdentificacion,
      FechaIngreso: fecha,
      IdArea: pEmpleado.idArea,
    };

    this.bloqueado = true;
    this.empleadosService.guardarEmpleado(vObj).subscribe(
      (data) => {
        this.respuesta = data;
        this.bloqueado = false;
        if (!this.respuesta.isOk) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.empleado = [];
        this.getEmpleadosxFiltro();
        this.modalRef.hide();

        Swal.fire({
          icon: 'success',
          title: 'Éxito',
          text: "Registro almacenado con exito",
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que busca un empleado x Id y abre el modal de Editar
   * @param template
   * @param pEmpleado
   */
  public abrirModalEditar(template: TemplateRef<any>, pEmpleado: any) {
    const Id = pEmpleado.idEmpleado;
    this.empleadosService.getEmpleadoxId(Id).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.empleado = this.respuesta.retorno;
        if (this.empleado != null) {
          this.empleado.idArea = String(this.empleado.idArea);
          this.fechaIngresoEmpleado = this.empleado.fechaIngreso;
        }
        this.editable = true;
        this.modalRef = this.modalService.show(template, this.config);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que manda a editar los datos del Empleado
   * @param pEmpleado
   */
  public editar(pEmpleado) {
    if (pEmpleado.primerApellido == "" || pEmpleado.segundoApellido == "" || pEmpleado.primerNombre == "" ||
      pEmpleado.pais == null || pEmpleado.tipoIdentificacion == null || pEmpleado.numeroIdentificacion == "" ||
      pEmpleado.fechaIngreso == null || pEmpleado.idArea == null || pEmpleado.estado == null) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: "Debe diligenciar todos los campos"
      });
      return;
    }

    if (this.fechaIngresoEmpleado != pEmpleado.fechaIngreso) {
      var fechaIngreso = new Date(pEmpleado.fechaIngreso);
      var dia = fechaIngreso.getDate();
      var mes = fechaIngreso.getMonth() + 1;
      var anio = fechaIngreso.getFullYear();
      if (dia < 10) {
        var myDia = "0" + dia;
      } else {
        var myDia = dia.toString();
      }

      if (mes < 10) {
        var myMes = "0" + mes;
      } else {
        var myMes = mes.toString();
      }

      var fecha = myDia + "/" + myMes + "/" + anio;
    }
    const vObj = {
      IdEmpleado: pEmpleado.idEmpleado,
      PrimerApellido: pEmpleado.primerApellido,
      SegundoApellido: pEmpleado.segundoApellido,
      PrimerNombre: pEmpleado.primerNombre,
      OtrosNombres: pEmpleado.otrosNombres == null ? "" : pEmpleado.otrosNombres,
      Pais: pEmpleado.pais,
      TipoIdentificacion: pEmpleado.tipoIdentificacion,
      NumeroIdentificacion: pEmpleado.numeroIdentificacion,
      FechaIngreso: fecha,
      IdArea: pEmpleado.idArea,
      Estado: pEmpleado.estado
    };
    this.bloqueado = true;
    this.empleadosService.guardarEmpleado(vObj).subscribe(
      (data) => {
        this.respuesta = data;
        this.bloqueado = false;
        if (!this.respuesta.isOk) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }

        this.empleado = [];
        this.getEmpleadosxFiltro();
        this.modalRef.hide();

        Swal.fire({
          icon: 'success',
          title: 'Éxito',
          text: "Registro actualizado con exito",
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que abre la confirmacion del metodo Eliminar
   * @param pEmpleado
   */
  public abrirConfirmar(pEmpleado: any) {
    Swal.fire({
      title: '¿Está seguro de que desea eliminar el Empleado?',
      text: "Recuerde que esta acción no podrá ser modificada",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.isConfirmed) {
        this.eliminarEmpleado(pEmpleado);
      }
    })
  }

  /**
   * Funcion que elimina un Empleado
   * @param pEmpleado
   */
  public eliminarEmpleado(pEmpleado: any) {
    const Id = pEmpleado.idEmpleado;
    this.empleadosService.borrarEmpleado(Id).subscribe(
      (data) => {
        this.respuesta = data;

        if (!this.respuesta.isOk) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: this.respuesta.message
          });
          console.log(this.respuesta.message + " " + this.respuesta.messageTrace);
          return;
        }
        this.getEmpleadosxFiltro();

        Swal.fire({
          icon: 'success',
          title: 'Éxito',
          text: "Registro eliminado con exito",
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * Funcion que cambia de pagina
   * @param event
   */
  public pageChanged(event: any): void {
    this.page = event.page;
    this.getEmpleadosxFiltro();
  }

}
