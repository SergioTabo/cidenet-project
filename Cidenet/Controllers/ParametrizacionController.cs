﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using DataAccess.Conexiones.Contratos;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Shared;
using Shared.DTO;

namespace Cidenet.Controllers
{
    [Route("api/[controller]")]
    public class ParametrizacionController : ControllerBase
    {
        private IConfiguration Configuration;
        Conexion.Conexion ConexionBase;
        public IConexion _Conexion;

        private IHostingEnvironment _hostingEnvironment;

        public ParametrizacionController(IConfiguration iConfig, IHostingEnvironment hostingEnvironment)
        {
            try
            {
                Configuration = iConfig;
                _hostingEnvironment = hostingEnvironment;
                ConexionBase = new Conexion.Conexion(Configuration);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        /// <summary>
        /// Metodo que obtiene los elementos x Tipo
        /// </summary>
        /// <param name="pTipo"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ResultadoRetorno<IList<itemComboDTO>> getElementosxTipo(string pTipo)
        {
            ResultadoRetorno<IList<itemComboDTO>> retorno = new ResultadoRetorno<IList<itemComboDTO>>();
            try
            {
                this._Conexion = ConexionBase.getCnx();

                retorno = BLL_Generica.getElementosxTipo(this._Conexion, pTipo);
                this._Conexion.CerrarConexion();
                return retorno;
            }
            catch (Exception e)
            {
                retorno.SetError(e.Message);
                this._Conexion.CerrarConexion();
                return retorno;
            }
        }
    }
}