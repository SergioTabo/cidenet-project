﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Core.Implementacion;
using DataAccess.Conexiones.Contratos;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Shared;
using Shared.DTO;

namespace Cidenet.Controllers
{
    [Route("api/[controller]")]
    public class EmpleadoController : ControllerBase
    {
        private IConfiguration Configuration;
        Conexion.Conexion ConexionBase;
        public IConexion _Conexion;

        private IHostingEnvironment _hostingEnvironment;

        public EmpleadoController(IConfiguration iConfig, IHostingEnvironment hostingEnvironment)
        {
            try
            {
                Configuration = iConfig;
                _hostingEnvironment = hostingEnvironment;
                ConexionBase = new Conexion.Conexion(Configuration);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        /// <summary>
        /// Metodo que obtiene el listado de empleados x Filtro y paginados
        /// </summary>
        /// <param name="pFiltro"></param>
        /// <param name="pItemsPorPagina"></param>
        /// <param name="pPaginaActual"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ResultadoRetorno<List<itemEmpleadoDTO>> getEmpleadosxFiltro(string pFiltro, decimal pItemsPorPagina, decimal pPaginaActual)
        {
            ResultadoRetorno<List<itemEmpleadoDTO>> retorno = new ResultadoRetorno<List<itemEmpleadoDTO>>();
            try
            {
                this._Conexion = ConexionBase.getCnx();

                retorno = BLL_Empleado.getEmpleadosxFiltro(this._Conexion, pFiltro, pItemsPorPagina, pPaginaActual);
                this._Conexion.CerrarConexion();
                return retorno;
            }
            catch (Exception e)
            {
                retorno.SetError(e.Message);
                this._Conexion.CerrarConexion();
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que almacena o actualiza un empleado
        /// </summary>
        /// <param name="pEmpleado"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public ResultadoRetorno<decimal> guardarEmpleado([FromBody]itemEmpleadoDTO pEmpleado)
        {
            ResultadoRetorno<decimal> retorno = new ResultadoRetorno<decimal>();
            try
            {
                this._Conexion = ConexionBase.getCnx();

                retorno = BLL_Empleado.guardarEmpleado(this._Conexion, pEmpleado);
                this._Conexion.CerrarConexion();
                return retorno;
            }
            catch (Exception e)
            {
                retorno.SetError(e.Message);
                this._Conexion.CerrarConexion();
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que obtiene un Empleado x Id
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ResultadoRetorno<itemEmpleadoDTO> getEmpleadoxId(decimal pId)
        {
            ResultadoRetorno<itemEmpleadoDTO> retorno = new ResultadoRetorno<itemEmpleadoDTO>();
            try
            {
                this._Conexion = ConexionBase.getCnx();

                retorno = BLL_Empleado.getEmpleadoxId(this._Conexion, pId);
                this._Conexion.CerrarConexion();
                return retorno;
            }
            catch (Exception e)
            {
                retorno.SetError(e.Message);
                this._Conexion.CerrarConexion();
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que elimina un empleado x Id
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public ResultadoRetorno<string> eliminarEmpleado(decimal pId)
        {
            ResultadoRetorno<string> retorno = new ResultadoRetorno<string>();
            try
            {
                this._Conexion = ConexionBase.getCnx();

                retorno = BLL_Empleado.eliminarEmpleado(this._Conexion, pId);
                this._Conexion.CerrarConexion();
                return retorno;
            }
            catch (Exception e)
            {
                retorno.SetError(e.Message);
                this._Conexion.CerrarConexion();
                return retorno;
            }
        }
    }
}