﻿using DataAccess.Conexiones.Contratos;
using DataAccess.Conexiones.Implementacion;
using Microsoft.Extensions.Configuration;
using Shared.DTO;

namespace Cidenet.Conexion
{
    public class Conexion
    {
        private IConfiguration Configuration;
        private CredencialesDTO credenciales;


        public Conexion(IConfiguration iConfig)
        {
            Configuration = iConfig;
        }

        public IConexion getCnx()
        {
            credenciales = new CredencialesDTO();
            credenciales.Host = Configuration.GetValue<string>("Credenciales:Host");
            credenciales.Usuario = Configuration.GetValue<string>("Credenciales:Usuario");
            credenciales.Clave = Configuration.GetValue<string>("Credenciales:Clave");
            credenciales.Puerto = Configuration.GetValue<string>("Credenciales:Puerto");
            credenciales.BaseDeDatos = Configuration.GetValue<string>("Credenciales:BaseDatos");

            IConexion cnx = new ConexionSqlServer(credenciales);
            cnx.Conectar();
            return cnx;
        }
    }
}
