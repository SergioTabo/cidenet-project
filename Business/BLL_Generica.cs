﻿using DataAccess;
using DataAccess.Conexiones.Contratos;
using Shared;
using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;

namespace Business
{
    public class BLL_Generica: META_BLL
    {
        /// <summary>
        /// Metodo que obtiene los datos parametricos x Tipo
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pTipo"></param>
        /// <returns></returns>
        public static ResultadoRetorno<IList<itemComboDTO>> getElementosxTipo(IConexion pConexion, string pTipo)
        {
            ResultadoRetorno<IList<itemComboDTO>> retorno = new ResultadoRetorno<IList<itemComboDTO>>();
            try
            {
                IList<itemComboDTO> vLista = new List<itemComboDTO>();
                DAL_Generica DAL = new DAL_Generica(pConexion);
                DataTable dt = DAL.getElementosxTipo(pTipo);
                foreach (DataRow itemRow in dt.Rows)
                {
                    vLista.Add(
                    new itemComboDTO()
                    {
                        Valor = itemRow[0].ToString(),
                        Nombre = itemRow[1].ToString()
                    }
                    );
                }
                retorno.Retorno = vLista;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }
    }
}
