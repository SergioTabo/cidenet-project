﻿using Business.Core.Contratos;
using DataAccess.Conexiones.Contratos;
using DataAccess.Core.Implementacion;
using Shared;
using Shared.DTO;
using System;
using System.Collections.Generic;
using System.Data;

namespace Business.Core.Implementacion
{
    public class BLL_Empleado : MBLL_Empleado, IBLL_Empleado
    {
        #region CONSTRUCTORES
        public BLL_Empleado(IConexion conexion)
        {
            this._Conexion = conexion;
        }

        public BLL_Empleado()
        {
        }
        #endregion CONSTRUCTORES

        #region METODOS ESTATICOS META
        public static new ResultadoRetorno<BLL_Empleado> getEntidadXUid(IConexion pConexion, decimal pUid)
        {
            return MBLL_Empleado.getEntidadXUid(pConexion, pUid);
        }

        public static new ResultadoRetorno<IList<BLL_Empleado>> ListaEmpleado(IConexion pConexion)
        {
            return MBLL_Empleado.ListaEmpleado(pConexion);
        }
        #endregion METODOS ESTATICOS META

        /// <summary>
        /// Metodo que obtiene los Empleados x Filtro
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pFiltro"></param>
        /// <returns></returns>
        public static ResultadoRetorno<List<itemEmpleadoDTO>> getEmpleadosxFiltro(IConexion pConexion, string pFiltro, decimal pItemsPorPagina, decimal pPaginaActual)
        {
            ResultadoRetorno<List<itemEmpleadoDTO>> retorno = new ResultadoRetorno<List<itemEmpleadoDTO>>();
            try
            {
                List<itemEmpleadoDTO> vLista = new List<itemEmpleadoDTO>();
                DAL_Empleado vDAL_Empleado = new DAL_Empleado(pConexion);
                DataTable dt = vDAL_Empleado.getEmpleadosxFiltro(pFiltro, pItemsPorPagina, pPaginaActual);
                DataRow dr;
                if (dt.Rows.Count != 0)
                {
                    foreach (DataRow itemRow in dt.Rows)
                    {
                        vLista.Add(
                        new itemEmpleadoDTO()
                        {
                            IdEmpleado = decimal.Parse(itemRow[0].ToString()),
                            PrimerApellido = itemRow[1].ToString(),
                            SegundoApellido = itemRow[2].ToString(),
                            PrimerNombre = itemRow[3].ToString(),
                            OtrosNombres = itemRow[4].ToString(),
                            Pais = itemRow[5].ToString(),
                            TipoIdentificacion = itemRow[6].ToString(),
                            NumeroIdentificacion = itemRow[7].ToString(),
                            CorreoElectronico = itemRow[8].ToString(),
                            IdArea = decimal.Parse(itemRow[9].ToString()),
                            Estado = itemRow[10].ToString(),
                            NombreEstado = itemRow[11].ToString(),
                            NombreTipoIdentificacion = itemRow[12].ToString(),
                            NombrePais = itemRow[13].ToString(),
                            NombreArea = itemRow[14].ToString(),
                        }
                        );
                    }
                }
                retorno.Retorno = vLista;

                dr = vDAL_Empleado.getTotalElementos(pFiltro);
                retorno.TotalElementos = decimal.Parse(dr[0].ToString());
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que almacena o actualiza un Empleado
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pEmpleado"></param>
        /// <returns></returns>
        public static ResultadoRetorno<decimal> guardarEmpleado(IConexion pConexion, itemEmpleadoDTO pEmpleado)
        {
            ResultadoRetorno<decimal> vRetorno = new ResultadoRetorno<decimal>();
            try
            {
                ResultadoRetorno<decimal> vRetornoAdi;
                ResultadoRetorno<string> vRetornoMod;

                BLL_Empleado vBLL_Empleado = new BLL_Empleado();
                ResultadoRetorno<BLL_Empleado> vRetornoEmpleado = new ResultadoRetorno<BLL_Empleado>();

                string vhoy = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                string vDominio;


                if (pEmpleado.IdEmpleado == -1)
                {
                    vRetornoEmpleado = BLL_Empleado.getEmpleadoxIdentificacion(pConexion, pEmpleado.NumeroIdentificacion, pEmpleado.TipoIdentificacion);

                    if (vRetornoEmpleado.Retorno != null)
                    {
                        vRetorno.SetError("Número de Identificación ya registrado");
                        return vRetorno;
                    }

                    if (pEmpleado.Pais == Paises.Colombia)
                    {
                        vDominio = Dominios.Colombia;
                    }
                    else
                    {
                        vDominio = Dominios.EstadosUnidos;
                    }

                    pEmpleado.CorreoElectronico = pEmpleado.PrimerNombre.ToLower().Replace(" ", "") + "." + pEmpleado.PrimerApellido.ToLower().Replace(" ", "") + "@cidenet.com." + vDominio;

                    bool vExiste = true;
                    int vInicio = 1;
                    while (vExiste == true)
                    {
                        vRetornoEmpleado = BLL_Empleado.getEmpleadoxCorreoElectronico(pConexion, pEmpleado.CorreoElectronico);

                        if (vRetornoEmpleado.Retorno != null)
                        {
                            var vArray = pEmpleado.CorreoElectronico.Split('@');
                            vArray[0] = vArray[0] + "." + vInicio;
                            pEmpleado.CorreoElectronico = vArray[0] + "@" + vArray[1];
                            vExiste = true;
                            vInicio++;
                        }
                        else
                        {
                            vExiste = false;
                        }
                    }

                    vBLL_Empleado = new BLL_Empleado()
                    {
                        IdEmpleado = -1,
                        PrimerApellido = pEmpleado.PrimerApellido.ToUpper(),
                        SegundoApellido = pEmpleado.SegundoApellido.ToUpper(),
                        PrimerNombre = pEmpleado.PrimerNombre.ToUpper(),
                        OtrosNombres = pEmpleado.OtrosNombres == null ? "" : pEmpleado.OtrosNombres.ToUpper(),
                        Pais = pEmpleado.Pais,
                        TipoIdentificacion = pEmpleado.TipoIdentificacion,
                        NumeroIdentificacion = pEmpleado.NumeroIdentificacion,
                        CorreoElectronico = pEmpleado.CorreoElectronico,
                        FechaIngreso = SharedFunciones.fechaCortaVisualizacionABD(pEmpleado.FechaIngreso),
                        IdArea = pEmpleado.IdArea,
                        Estado = "A",
                        FechaHoraRegistro = SharedFunciones.fechaCortaVisualizacionABD(vhoy),
                        FechaHoraEdicion = SharedFunciones.fechaCortaVisualizacionABD(vhoy),
                    };

                    vRetornoAdi = vBLL_Empleado.Adicionar(pConexion);

                    if (!vRetornoAdi.IsOk)
                    {
                        vRetorno.SetError(vRetornoAdi.Message);
                        return vRetorno;
                    }

                    vRetorno.Retorno = vRetornoAdi.Retorno;
                }
                else
                {

                    vRetornoEmpleado = BLL_Empleado.getEntidadXUid(pConexion, pEmpleado.IdEmpleado);
                    if (!vRetornoEmpleado.IsOk)
                    {
                        vRetorno.SetError("Error al editar el empleado: " + vRetorno.Message);
                        return vRetorno;
                    }

                    vBLL_Empleado = vRetornoEmpleado.Retorno;

                    if (pEmpleado.NumeroIdentificacion != vBLL_Empleado.NumeroIdentificacion)
                    {
                        vRetornoEmpleado = BLL_Empleado.getEmpleadoxIdentificacion(pConexion, pEmpleado.NumeroIdentificacion, pEmpleado.TipoIdentificacion);

                        if (vRetornoEmpleado.Retorno != null)
                        {
                            vRetorno.SetError("Número de Identificación ya registrado");
                            return vRetorno;
                        }
                    }

                    if (pEmpleado.PrimerApellido.ToUpper() != vBLL_Empleado.PrimerApellido || pEmpleado.PrimerNombre.ToUpper() != vBLL_Empleado.PrimerNombre || pEmpleado.Pais != vBLL_Empleado.Pais)
                    {
                        if (pEmpleado.Pais == Paises.Colombia)
                        {
                            vDominio = Dominios.Colombia;
                        }
                        else
                        {
                            vDominio = Dominios.EstadosUnidos;
                        }

                        pEmpleado.CorreoElectronico = pEmpleado.PrimerNombre.ToLower().Replace(" ", "") + "." + pEmpleado.PrimerApellido.ToLower().Replace(" ", "") + "@cidenet.com." + vDominio;

                        bool vExiste = true;
                        int vInicio = 1;
                        while (vExiste == true)
                        {
                            vRetornoEmpleado = BLL_Empleado.getEmpleadoxCorreoElectronico(pConexion, pEmpleado.CorreoElectronico);

                            if (vRetornoEmpleado.Retorno != null)
                            {
                                var vArray = pEmpleado.CorreoElectronico.Split('@');
                                vArray[0] = vArray[0] + "." + vInicio;
                                pEmpleado.CorreoElectronico = vArray[0] + "@" + vArray[1];
                                vExiste = true;
                                vInicio++;
                            }
                            else
                            {
                                vExiste = false;
                            }
                        }
                    }

                    vBLL_Empleado.PrimerApellido = pEmpleado.PrimerApellido.ToUpper();
                    vBLL_Empleado.SegundoApellido = pEmpleado.SegundoApellido.ToUpper();
                    vBLL_Empleado.PrimerNombre = pEmpleado.PrimerNombre.ToUpper();
                    vBLL_Empleado.OtrosNombres = pEmpleado.OtrosNombres == null ? vBLL_Empleado.OtrosNombres : pEmpleado.OtrosNombres.ToUpper();
                    vBLL_Empleado.Pais = pEmpleado.Pais;
                    vBLL_Empleado.TipoIdentificacion = pEmpleado.TipoIdentificacion;
                    vBLL_Empleado.NumeroIdentificacion = pEmpleado.NumeroIdentificacion;
                    vBLL_Empleado.CorreoElectronico = pEmpleado.CorreoElectronico == null ? vBLL_Empleado.CorreoElectronico : pEmpleado.CorreoElectronico;
                    vBLL_Empleado.FechaIngreso =  pEmpleado.FechaIngreso == null ? vBLL_Empleado.FechaIngreso : SharedFunciones.fechaCortaVisualizacionABD(pEmpleado.FechaIngreso);
                    vBLL_Empleado.IdArea = pEmpleado.IdArea;
                    vBLL_Empleado.Estado = pEmpleado.Estado == null ? vBLL_Empleado.Estado : pEmpleado.Estado;
                    vBLL_Empleado.FechaHoraEdicion = SharedFunciones.fechaCortaVisualizacionABD(vhoy);


                    vRetornoMod = vBLL_Empleado.Actualizar(pConexion);

                    if (!vRetornoMod.IsOk)
                    {
                        vRetorno.SetError(vRetornoMod.Message);
                        return vRetorno;
                    }

                    vRetorno.Retorno = vBLL_Empleado.IdEmpleado;
                }

                return vRetorno;
            }
            catch (Exception e)
            {
                vRetorno.SetError("Se ha presentado un error: " + e.Message);
                return vRetorno;
            }
        }

        /// <summary>
        /// Metodo que obtiene un empleado x Identificacion
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pIdentificacion"></param>
        /// <returns></returns>
        public static ResultadoRetorno<BLL_Empleado> getEmpleadoxIdentificacion(IConexion pConexion, string pIdentificacion, string pTipoIdentificacion)
        {
            ResultadoRetorno<BLL_Empleado> retorno = new ResultadoRetorno<BLL_Empleado>();
            try
            {

                IList<BLL_Empleado> vLista = new List<BLL_Empleado>();
                DAL_Empleado DAL = new DAL_Empleado(pConexion);
                DataRow itemRow = DAL.getEmpleadoxIdentificacion(pIdentificacion, pTipoIdentificacion);

                retorno.Retorno = new BLL_Empleado()
                {
                    IdEmpleado = decimal.Parse(itemRow[0].ToString()),
                    PrimerApellido = itemRow[1].ToString(),
                    SegundoApellido = itemRow[2].ToString(),
                    PrimerNombre = itemRow[3].ToString(),
                    OtrosNombres = itemRow[4].ToString(),
                    Pais = itemRow[5].ToString(),
                    TipoIdentificacion = itemRow[6].ToString(),
                    NumeroIdentificacion = itemRow[7].ToString(),
                    CorreoElectronico = itemRow[8].ToString(),
                    FechaIngreso = itemRow[9].ToString(),
                    IdArea = decimal.Parse(itemRow[10].ToString()),
                    Estado = itemRow[11].ToString(),
                    FechaHoraRegistro = itemRow[12].ToString(),
                    FechaHoraEdicion = itemRow[13].ToString()
                };
                return retorno;

            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que obtiene un Empleado x Correo
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pCorreo"></param>
        /// <returns></returns>
        public static ResultadoRetorno<BLL_Empleado> getEmpleadoxCorreoElectronico(IConexion pConexion, string pCorreo)
        {
            ResultadoRetorno<BLL_Empleado> retorno = new ResultadoRetorno<BLL_Empleado>();
            try
            {

                IList<BLL_Empleado> vLista = new List<BLL_Empleado>();
                DAL_Empleado DAL = new DAL_Empleado(pConexion);
                DataRow itemRow = DAL.getEmpleadoxCorreoElectronico(pCorreo);

                retorno.Retorno = new BLL_Empleado()
                {
                    IdEmpleado = decimal.Parse(itemRow[0].ToString()),
                    PrimerApellido = itemRow[1].ToString(),
                    SegundoApellido = itemRow[2].ToString(),
                    PrimerNombre = itemRow[3].ToString(),
                    OtrosNombres = itemRow[4].ToString(),
                    Pais = itemRow[5].ToString(),
                    TipoIdentificacion = itemRow[6].ToString(),
                    NumeroIdentificacion = itemRow[7].ToString(),
                    CorreoElectronico = itemRow[8].ToString(),
                    FechaIngreso = itemRow[9].ToString(),
                    IdArea = decimal.Parse(itemRow[10].ToString()),
                    Estado = itemRow[11].ToString(),
                    FechaHoraRegistro = itemRow[12].ToString(),
                    FechaHoraEdicion = itemRow[13].ToString()
                };
                return retorno;

            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que obtiene un Empleado x Id
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pUid"></param>
        /// <returns></returns>
        public static ResultadoRetorno<itemEmpleadoDTO> getEmpleadoxId(IConexion pConexion, decimal pUid)
        {
            ResultadoRetorno<itemEmpleadoDTO> retorno = new ResultadoRetorno<itemEmpleadoDTO>();
            try
            {

                DAL_Empleado DAL = new DAL_Empleado(pConexion);
                DataRow itemRow = DAL.getEntidadXUid(pUid);

                retorno.Retorno = new itemEmpleadoDTO()
                {
                    IdEmpleado = decimal.Parse(itemRow[0].ToString()),
                    PrimerApellido = itemRow[1].ToString(),
                    SegundoApellido = itemRow[2].ToString(),
                    PrimerNombre = itemRow[3].ToString(),
                    OtrosNombres = itemRow[4].ToString(),
                    Pais = itemRow[5].ToString(),
                    TipoIdentificacion = itemRow[6].ToString(),
                    NumeroIdentificacion = itemRow[7].ToString(),
                    CorreoElectronico = itemRow[8].ToString(),
                    FechaIngreso = SharedFunciones.fechaCortaBDAVisualizacion(itemRow[9].ToString()),
                    IdArea = decimal.Parse(itemRow[10].ToString()),
                    Estado = itemRow[11].ToString(),
                    FechaHoraRegistro = itemRow[12].ToString(),
                    FechaHoraEdicion = itemRow[13].ToString()
                };
                return retorno;

            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        /// <summary>
        /// Metodo que elimina un Empleado
        /// </summary>
        /// <param name="pConexion"></param>
        /// <param name="pId"></param>
        /// <returns></returns>
        public static ResultadoRetorno<string> eliminarEmpleado(IConexion pConexion, decimal pId)
        {
            ResultadoRetorno<string> retorno = new ResultadoRetorno<string>();
            try
            {
                ResultadoRetorno<string> vRetornoEli;
                BLL_Empleado vBLL_Empleado = new BLL_Empleado();
                ResultadoRetorno<BLL_Empleado> vRetornoEmpleado = new ResultadoRetorno<BLL_Empleado>();

                vRetornoEmpleado = BLL_Empleado.getEntidadXUid(pConexion, pId);
                if (!vRetornoEmpleado.IsOk)
                {
                    retorno.SetError("Error al eliminar el empleado: " + retorno.Message);
                    return retorno;
                }

                vBLL_Empleado = vRetornoEmpleado.Retorno;

                vRetornoEli = vBLL_Empleado.Eliminar(pConexion);
                if (!vRetornoEli.IsOk)
                {
                    retorno.SetError(vRetornoEli.Message);
                    return retorno;
                }

                return retorno;

            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }
    }
}