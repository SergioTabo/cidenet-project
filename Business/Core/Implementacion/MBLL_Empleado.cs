﻿using DataAccess.Conexiones.Contratos;
using DataAccess.Core.Contratos;
using DataAccess.Core.Implementacion;
using Shared;
using System;
using System.Collections.Generic;
using System.Data;

namespace Business.Core.Implementacion
{
    public class MBLL_Empleado : META_BLL
    {
        #region CONSTRUCTORES
        public MBLL_Empleado(IConexion conexion)
        {
            this._Conexion = conexion;
        }

        public MBLL_Empleado()
        {
        }
        #endregion CONSTRUCTORES


        #region ATRIBUTOS
        public decimal IdEmpleado { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string PrimerNombre { get; set; }
        public string OtrosNombres { get; set; }
        public string Pais { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string CorreoElectronico { get; set; }
        public string FechaIngreso { get; set; }
        public decimal IdArea { get; set; }
        public string Estado { get; set; }
        public string FechaHoraRegistro { get; set; }
        public string FechaHoraEdicion { get; set; }

        #endregion ATRIBUTOS

        #region OPERACIONES BASICAS
        public static ResultadoRetorno<BLL_Empleado> getEntidadXUid(IConexion pConexion, decimal pUid)
        {
            ResultadoRetorno<BLL_Empleado> retorno = new ResultadoRetorno<BLL_Empleado>();
            try
            {

                IList<BLL_Empleado> vLista = new List<BLL_Empleado>();
                IDAL_Empleado DAL = new DAL_Empleado(pConexion);
                DataRow itemRow = DAL.getEntidadXUid(pUid);

                retorno.Retorno = new BLL_Empleado()
                {
                    IdEmpleado = decimal.Parse(itemRow[0].ToString()),
                    PrimerApellido = itemRow[1].ToString(),
                    SegundoApellido = itemRow[2].ToString(),
                    PrimerNombre = itemRow[3].ToString(),
                    OtrosNombres = itemRow[4].ToString(),
                    Pais = itemRow[5].ToString(),
                    TipoIdentificacion = itemRow[6].ToString(),
                    NumeroIdentificacion = itemRow[7].ToString(),
                    CorreoElectronico = itemRow[8].ToString(),
                    FechaIngreso = itemRow[9].ToString(),
                    IdArea = decimal.Parse(itemRow[10].ToString()),
                    Estado = itemRow[11].ToString(),
                    FechaHoraRegistro = itemRow[12].ToString(),
                    FechaHoraEdicion = itemRow[13].ToString()
                };
                return retorno;

            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        public static ResultadoRetorno<IList<BLL_Empleado>> ListaEmpleado(IConexion pConexion)
        {
            ResultadoRetorno<IList<BLL_Empleado>> retorno = new ResultadoRetorno<IList<BLL_Empleado>>();
            try
            {
                IList<BLL_Empleado> vLista = new List<BLL_Empleado>();
                IDAL_Empleado DAL = new DAL_Empleado(pConexion);
                DataTable dt = DAL.ListaEmpleado();
                foreach (DataRow itemRow in dt.Rows)
                {
                    vLista.Add(
                    new BLL_Empleado()
                    {
                        IdEmpleado = decimal.Parse(itemRow[0].ToString()),
                        PrimerApellido = itemRow[1].ToString(),
                        SegundoApellido = itemRow[2].ToString(),
                        PrimerNombre = itemRow[3].ToString(),
                        OtrosNombres = itemRow[4].ToString(),
                        Pais = itemRow[5].ToString(),
                        TipoIdentificacion = itemRow[6].ToString(),
                        NumeroIdentificacion = itemRow[7].ToString(),
                        CorreoElectronico = itemRow[8].ToString(),
                        FechaIngreso = itemRow[9].ToString(),
                        IdArea = decimal.Parse(itemRow[10].ToString()),
                        Estado = itemRow[11].ToString(),
                        FechaHoraRegistro = itemRow[12].ToString(),
                        FechaHoraEdicion = itemRow[13].ToString()
                    }
                    );
                }
                retorno.Retorno = vLista;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        public ResultadoRetorno<decimal> Adicionar(IConexion pConexion)
        {
            ResultadoRetorno<decimal> retorno = new ResultadoRetorno<decimal>();
            try
            {
                IDAL_Empleado DAL = new DAL_Empleado(pConexion);
                decimal vUid = DAL.insert(this.IdEmpleado, this.PrimerApellido, this.SegundoApellido, this.PrimerNombre, this.OtrosNombres, this.Pais, this.TipoIdentificacion, this.NumeroIdentificacion, this.CorreoElectronico, this.FechaIngreso, this.IdArea, this.Estado, this.FechaHoraRegistro, this.FechaHoraEdicion);
                this.IdEmpleado = vUid;
                retorno.Retorno = vUid;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        public ResultadoRetorno<string> Actualizar(IConexion pConexion)
        {
            ResultadoRetorno<string> retorno = new ResultadoRetorno<string>();
            try
            {
                IDAL_Empleado DAL = new DAL_Empleado(pConexion);
                DAL.update(this.IdEmpleado, this.PrimerApellido, this.SegundoApellido, this.PrimerNombre, this.OtrosNombres, this.Pais, this.TipoIdentificacion, this.NumeroIdentificacion, this.CorreoElectronico, this.FechaIngreso, this.IdArea, this.Estado, this.FechaHoraRegistro, this.FechaHoraEdicion);
                retorno.Retorno = SharedConstants.OK;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }

        public ResultadoRetorno<string> Eliminar(IConexion pConexion)
        {
            ResultadoRetorno<string> retorno = new ResultadoRetorno<string>();
            try
            {
                IDAL_Empleado DAL = new DAL_Empleado(pConexion);
                DAL.delete(this.IdEmpleado);
                retorno.Retorno = SharedConstants.OK;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.SetError(ex.Message, ex.StackTrace);
                return retorno;
            }
        }
        #endregion OPERACIONES BASICAS
    }
}
