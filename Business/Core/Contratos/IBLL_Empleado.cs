﻿using DataAccess.Conexiones.Contratos;
using Shared;
using System;

namespace Business.Core.Contratos
{
    public interface IBLL_Empleado
    {
        #region OPERACIONES_BASICAS
        ResultadoRetorno<decimal> Adicionar(IConexion pConexion);
        ResultadoRetorno<string> Actualizar(IConexion pConexion);
        ResultadoRetorno<string> Eliminar(IConexion pConexion);
        #endregion OPERACIONES_BASICAS
    }
}
